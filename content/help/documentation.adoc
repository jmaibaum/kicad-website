+++
title = "Documentation"
+++

The documentation index have moved to a new location at:
https://docs.kicad.org
