+++
title = "Download Libraries"
summary = "Information on how to obtain KiCad library updates for various KiCad versions"
[menu.main]
    parent = "Libraries"
    name = "Download"
    url = "/libraries/download/"
    identifier = "libraries_download"
    weight = 10
+++

== Library Downloads for KiCad 6.x

Libraries are included along with the KiCad installer or packages for major operating systems. Most users should use the libraries packaged for their operating system and do not need the information below. Refer to the link:/download/[download page for your operating system] for more information.

== GitLab Libraries

KiCad libraries are community contributed and hosted on GitLab at link:https://gitlab.com/kicad/libraries[gitlab.com/kicad/libraries]. Users who wish to make contributions to the libraries can link:https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#fork[fork] the library repositories. If you wish to contribute to the libraries, refer to the link:/libraries/contribute/[contribution guide].

It is also possible to keep your system libraries up to date with the latest additions by link:https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository[cloning] the library repositories using Git. Tracking the library repositories using Git means that only the __changes__ to the libraries need to be downloaded, rather than retrieving the entire library set each time.

=== Library Repositories for KiCad 6.x

For KiCad version 6.x, the libraries are organised into four separate repositories on GitLab: 

* Schematic symbols: `link:https://gitlab.com/kicad/libraries/kicad-symbols[kicad-symbols]`
* PCB footprints: `link:https://gitlab.com/kicad/libraries/kicad-footprints[kicad-footprints]`
* 3D models: `link:https://gitlab.com/kicad/libraries/kicad-packages3d[kicad-packages3d]`
** Source files for 3D models: `link:https://gitlab.com/kicad/libraries/kicad-packages3d-source[kicad-packages3d-source]`
* Project templates: `link:https://gitlab.com/kicad/libraries/kicad-templates[kicad-templates]`

== Library Downloads for Old KiCad Versions

Libraries for KiCad 5.x and earlier are no longer maintained. Archives of these libraries are available to download, and read-only GitHub repositories are available.

=== KiCad 5.x

KiCad 5.x libraries are available for download at link:https://kicad.github.io[https://kicad.github.io]. Library data are provided as compressed archives.

* Schematic symbols: link:https://kicad.github.io/symbols[https://kicad.github.io/symbols]
* PCB footprints: link:https://kicad.github.io/footprints[https://kicad.github.io/footprints]
* 3D models: link:https://kicad.github.io/packages3d[https://kicad.github.io/packages3d]

The KiCad 5.x libraries are also available via their GitHub repositories. The libraries are no longer maintained, and the repositories have been archived to make them read-only.

* Schematic symbols: `link:https://github.com/KiCad/kicad-symbols[kicad-symbols]`
* PCB footprints: `link:https://github.com/KiCad/kicad-footprints[kicad-footprints]`
* 3D models: `link:https://github.com/KiCad/kicad-packages3d[kicad-packages3d]`
* Project templates: `link:https://github.com/KiCad/kicad-templates[kicad-templates]`

=== KiCad 4.x

Snapshots of the libraries aligned with the minor KiCad 4.x releases can be found in the link:https://kicad-downloads.s3.cern.ch/index.html?prefix=libraries/[KiCad CERN archive].

* Schematic symbols, 3D models, and project templates: link:https://kicad-downloads.s3.cern.ch/index.html?prefix=libraries/[`kicad-library-*` archives]
* PCB footprints: link:https://kicad-downloads.s3.cern.ch/index.html?prefix=libraries/[`kicad-footprints-*` archives]

The KiCad 4.x libraries are also available via their GitHub repositories. The libraries are no longer maintained and the repositories have been archived to make them read-only.

* Schematic symbols, 3D models, and project templates: `link:https://github.com/kicad/kicad-library[kicad-library]`
* PCB footprints: link:https://github.com/kicad?&q=.pretty[individual `.pretty` repositories]